import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
// import { GenderPipe } from 'src/app/pipe/gender.pipe';
import { Gender, User, UserService } from 'src/app/services/user.service';

@Component({
  selector: 'lab-js-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public user$: Observable<User>;
  public id: number;
  public firstName: string;
  public lastName: string;
  public userGender: Gender;
  public constructor(
    private readonly userService: UserService,
  ) { }

  public ngOnInit(): void {
    this.user$ = this.userService.getCurrentUser$();
    this.user$.subscribe(({id, firstName, lastName, gender}) => {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.userGender = gender;
    })
  }

}
